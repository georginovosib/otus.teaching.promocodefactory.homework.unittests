﻿using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private PartnersController _partnersController;
        private Mock<IRepository<Partner>> _partnersRepositoryMock = new Mock<IRepository<Partner>>();

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _partnersController = new PartnersController(_partnersRepositoryMock.Object);
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                NumberIssuedPromoCodes = 50,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116311"),
                        CreateDate = new DateTime(2022, 01, 09),
                        EndDate = new DateTime(2022, 02, 09),
                        CancelDate = new DateTime(2022, 02, 07),
                        Limit = 100
                    },
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116322"),
                        CreateDate = new DateTime(2022, 02, 07),
                        EndDate = new DateTime(2022, 08, 07),
                        CancelDate = null,
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        public SetPartnerPromoCodeLimitRequest CreateLimit()
        {
            var limit = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 1,
                EndDate = DateTime.Now.AddMonths(3)
            };

            return limit;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), 
                new SetPartnerPromoCodeLimitRequest());

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            Partner partner = new Partner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), 
                new SetPartnerPromoCodeLimitRequest());

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasLimit_NumberIssuedPromoCodes_Should_Be_0()
        {
            // Arrange
            var partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner); 

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), 
                new SetPartnerPromoCodeLimitRequest());

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetPartnerLimitPromoCode_CancelDate_Should_Be_Null()
        {
            // Arrange
            var partner = CreateBasePartner();
            var limitRequest = CreateLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), limitRequest);

            // Assert
            partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue).CancelDate.Should().BeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetPartnerLimitPromoCode_Limit_Should_More_0()
        {
            // Arrange
            var partner = CreateBasePartner();
            var limitRequest = CreateLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), limitRequest);

            // Assert
            partner.PartnerLimits.FirstOrDefault(x =>!x.CancelDate.HasValue).Limit.Should().BeGreaterThan(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_WhenAddingLimit_UpdateAsyncIncrementCountByOne()
        {
            // Arrange
            var partner = CreateBasePartner();
            var limitRequest = CreateLimit();
            var countPartnerLimits = partner.PartnerLimits.Count;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(It.IsAny<Guid>(), limitRequest);

            // Assert
            partner.PartnerLimits.Count.Should().Be(countPartnerLimits + 1);
        }
        //TODO: Add Unit Tests
    }
}